# Voice Assistant

This is a simple voice assistant program created using Python.

## Description

The voice assistant listens to voice commands and responds accordingly. It uses speech recognition to understand commands and then executes predefined actions based on those commands.

## Getting Started

### Prerequisites

Make sure you have Python installed on your system. If Python is not installed, download and install it from [Python's official website](https://www.python.org/).

### Installation

1. Clone or download this repository to your local machine.
2. Install the required Python libraries using the following command:
   
   ```bash
   pip install SpeechRecognition pyttsx3
   ```

### Usage

1. Open the terminal or command prompt.
2. Navigate to the directory where the voice assistant files are located.
3. Run the voice assistant by executing the following command:

    ```bash
    cd path/to/voice_assistant
    python voice_assistant.py
    ```

4. Once the program is running, the voice assistant will start listening for your voice commands. You can say commands like:
    - "Hello"
    - "How are you?"
    - "Goodbye"
5. The voice assistant will respond to your commands accordingly.

### Customization

You can customize the voice assistant's responses and add new functionalities by modifying the `voice_assistant.py` file. Feel free to explore and enhance the capabilities of the voice assistant according to your preferences.

### Contributing

Contributions are welcome! If you have suggestions, improvements, or new features to add, please feel free to fork this repository and create a pull request with your changes.

### License

This project is licensed under the MIT License.
