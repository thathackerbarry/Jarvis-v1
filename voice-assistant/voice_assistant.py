import speech_recognition as sr
import pyttsx3

# Initialize the recognizer
recognizer = sr.Recognizer()

# Initialize the text-to-speech engine
engine = pyttsx3.init()

# Function to speak out the given text
def speak(text):
    engine.say(text)
    engine.runAndWait()

# Function to listen to the user's voice command
def listen():
    with sr.Microphone() as source:
        print("Listening...")
        recognizer.adjust_for_ambient_noise(source)
        audio = recognizer.listen(source)

    try:
        print("Recognizing...")
        command = recognizer.recognize_google(audio).lower()
        print("You said:", command)
        return command
    except sr.UnknownValueError:
        print("Sorry, I didn't understand that.")
        return ""
    except sr.RequestError:
        print("Sorry, the service is unavailable.")
        return ""

# Function to execute commands
def execute_command(command):
    if "hello" in command:
        speak("Hello! How can I help you?")
    elif "how are you" in command:
        speak("I'm doing well, thank you!")
    elif "bye" in command:
        speak("Goodbye!")
        exit()
    elif "what is your name" in command:
        speak("I am Jarvis, your virtual assistant.")
    elif "tell me a joke" in command:
        speak("Why don't scientists trust atoms? Because they make up everything!")
    elif "weather" in command:
        speak("I'm sorry, I can't provide weather updates at the moment.")
        # Implement weather API integration here
    elif "news" in command:
        speak("I'm sorry, I can't fetch news updates at the moment.")
        # Implement news API integration here
    else:
        speak("I'm not sure how to help with that.")

# Main loop
while True:
    command = listen()
    execute_command(command)